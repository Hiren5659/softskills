# SOLID in 5 short examples

The theory of SOLID principles was introduced by [Robert C. Martin](https://en.wikipedia.org/wiki/Robert_C._Martin) in his 2000 paper [Design Principles and Design Patterns.](https://web.archive.org/web/20150906155800/http://www.objectmentor.com/resources/articles/Principles_and_Patterns.pdf)

The SOLID acronym was introduced later in 2004 or thereabouts by Michael Feathers.

## Introduction
SOLID is an acronym that stands for a set of principles which helps developers to create long-term maintainable source code that use an object oriented programming language.

### There are 5 principles :
    - Single responsibility principle
    - Open/closed principle
    - Liskov substitution principle
    - Interface segregation principle
    - Dependency inversion principle


## Single Responsibility principle
This principle is based on the fact that a class should be responsible of only one aspect of a program. By responsible we mean that the class can be impacted by a change in only one aspect of the program.

#### Violation of the Single Responsibility Principle in Ruby
```ruby
class MaraksMailer
  def initialize()
    #defination
  end

  def markscard_generater
    #definatoin
  end

  def markscard_sender
    # defination 
  end
end

markscard =MarksMailer.new()
markscard.markscard_generater
markscard.markscard_sender
```
The class MaraksMailer shown above performe two tasks: generate marks card and sending marks card. The class looks very simple as written. However, expanding this class in the future may be problematic, since we’ll likely have to change the logic of the class. The SRP principle tells us that a class should implement one single task, and therefore according to this principle we should divide the MaraksMailer class into two classes.

#### Correct use of the Single Responsibility Principle in Ruby
```ruby
class MarksCardGenerater
  def initialize()
    #defination
  end

  def generate
    #defination
  end
end

class MarksCardSender
  def initialize(mk)
    #defination
  end

  def send
    #defination
  end
end

mk = MarksCardGenerator.new().generate
MarksCardSender.new(mk).deliver
```
After refactoring, we have two classes that each perform a different task. If we wanted to expand the class responsible for marks card generation in the future, we could simply make the necessary changes without having to touch the MarksCardSender class.

## Open-Closed Principle
The Open-Closed Principle (OCP) states that *“Modules, classes, methods and other application entities should be open for extension but closed for modification.”* In simple words, all modules, classes, methods, etc. should be designed in such a way that you (or other developers) should able to change the behavior of the system without changing the source code.

Now we will see the code that not followed OCP. So every time developers need to add a new format or delivery option they have to modify the base code. 

#### Violation of the Open-Closed Principle in Ruby
```ruby
class Logger
  def initialize(format, delivery)
    #intialization
  end

  def log(string)
    #defination
  end

  private

  def format(string)
    case @format
    when :raw
      #statement
    when :with_date
      #statement
    when :with_date_and_details
      #statement
    else
      #statement
    end
  end

  def deliver(text)
    case @delivery
    when :by_email
      #statement
    when :by_sms
      #statement
    when :to_stdout
      #statement
    else
      #statement
    end
  end
end

logger = Logger.new(:raw, :by_sms)
logger.log('Emergency error! Please fix me!')
```

The code given below has the same feature as the code above. But when the developer has to add a new format or delivery option they don't have to modify the base code, they can just extend it.

#### Correct use of the Open-Closed Principle in Ruby
```ruby
class Logger
  def initialize(formatter: DateDetailsFormatter.new, sender: LogWriter.new)
    #intialization
  end

  def log(string)
    #defination
  end
end

class LogSms
  def initialize
    #intialization
  end

  def deliver(text)
    #defination
  end

  private

  def client
    #defination
  end
end

class LogMailer
  def initialize
   #intialization'
  end

  def deliver(text)
    #defination
  end
end

class LogWriter
  def deliver(log)
    #defination
  end
end

class DateFormatter
  def format(string)
    #defination
  end
end

class DateDetailsFormatter
  def format(string)
    #defination
  end
end

class RawFormatter
  def format(string)
    #defination
  end
end

logger = Logger.new(formatter: RawFormatter.new, sender: LogSms.new)
logger.log('Emergency error! Please fix me!')
```

## Liskov Substitution Principle
Many developers find the Liskov Substitution Principle (LSP) tangled and complicated. This is how Robert Martin defines the LSP principle: *“Subclasses should add to a base class’s behaviour, not replace it.”* In a more informal interpretation, the principle states that parent instances should be replaceable with one of their child instances without creating any unexpected or incorrect behaviour.

The code given below totally violating the LSP. The child class violates the LSP principle since it completely redefines the base class by returning a string with filtered data, whereas the base class returns an array of posts.


#### Violation of the Liskov Substitution Principle in Ruby
```ruby
class UserStatistic
  def initialize(user)
    @user = user
  end

  def posts
    @user.blog.posts
  end
end

class AdminStatistic < UserStatistic
  def posts
    user_posts = super

    string = ''
    user_posts.each do |post|
      string += "title: #{post.title} author: #{post.author}\n" if post.popular?
    end

    string
  end
end
```
Now let’s see how we refactored the code so it conforms to the Liskov substitution principle:

#### Correct use of the Liskov Substitution Principle in Ruby
```ruby
class UserStatistic
  def initialize(user)
    @user = user
  end

  def posts
    @user.blog.posts
  end
end

class AdminStatistic < UserStatistic
  def posts
    user_posts = super
    user_posts.select { |post| post.popular? }
  end

  def formatted_posts
    posts.map { |post| "title: #{post.title} author: #{post.author}" }.join("\n")
  end
end
```
To comply with the LSP principle, we can make two methods: “posts“ to filter post and “formatted_posts“ to generate a string. Therefore, the posts method returns the same type of data as the base class.

## The Interface Segregation Principle
The Interface Segregation Principle (ISP) state that: *“Clients shouldn’t depend on methods they don’t use. Several client-specific interfaces are better than one generalized interface.”*

In simply, main classes should be devided into smaller specific classes, so their clients only use methods they need. As a result, we get the different interfaces according to their purpose, so we avoid “fat” classes and code that’s hard to maintain. 
The ISP principle is best demonstrated with the piece of code. 

#### Violation of the Interface Segregation Principle in Ruby
```ruby
class CoffeeMachineInterface
  def select_drink_type
      # select drink type logic
  end

  def select_sugar_amount
     # select sugar logic
  end


  def clean_coffee_machine
    # clean coffee machine logic
  end

  def fill_coffee_beans
    # fill coffee beans logic
  end
end

class Person
  def initialize
    @coffee_machine = CoffeeMachineInterface.new
  end

  def make_coffee
    @coffee_machine.select_drink_type
    @coffee_machine.select_sugar_amount
   end
end

class Staff
  def initialize
    @coffee_machine = CoffeeMachineInterface.new
  end

  def serv
    @coffee_machine.clean_coffee_machine
    @coffee_machine.fill_coffee_beans
  end
end
```
In the example above, we have the code that represents the coffee vending machine interface. As you can see, the main class is accessed by both the User and Staff classes. In this case, the User methods are not useful for Staff and the Staff methods are not useful for Users. The ISP principle tells that one class should contain only the method it uses.

To make this example comply with the ISP principle, we created two interfaces: a separate user interface and a separate staff interface.

#### Correct use of the Interface Segregation Principle in Ruby
```ruby
class CoffeeMachineUserInterface
  def select_drink_type
      # select drink type logic
  end

  def select_portion
     # select portion logic
  end

  def select_sugar_amount
     # select sugar logic
  end

  def brew_coffee
     # brew coffee logic
  end
end

class CoffeeMachineServiceInterface
  def clean_coffee_machine
    # clean coffee machine logic
  end

  def fill_coffee_beans
    # fill coffee beans logic
  end

  def fill_water_supply
    # fill water logic
  end

  def fill_sugar_supply
    # fill sugar logic
  end
end

class Person
  def initialize
    @coffee_machine = CoffeeMachineUserInterface.new
  end

  def make_coffee
    @coffee_machine.select_drink_type
    @coffee_machine.select_portion
    @coffee_machine.select_sugar_amount
    @coffee_machine.brew_coffee
  end
end

class Staff
  def initialize
    @coffee_machine = CoffeeMachineServiceInterface.new
  end

  def serv
    @coffee_machine.clean_coffee_machine
    @coffee_machine.fill_coffee_beans
    @coffee_machine.fill_water_supply
    @coffee_machine.fill_sugar_supply
  end
end
```
For more info  [The Interface Segregation Principle (1996)](https://drive.google.com/file/d/0BwhCYaYDn8EgOTViYjJhYzMtMzYxMC00MzFjLWJjMzYtOGJiMDc5N2JkYmJi/view)

## The Dependency Inversion Principle
The Dependency Inversion Principle (DIP) suggests that *“High-level modules shouldn’t depend on low-level modules. Both modules should depend on abstractions. In addition, abstractions shouldn’t depend on details. Details depend on abstractions.”*


#### Violation of the Dependency Inversion Principle in Ruby
```ruby
class Newsperson
  def broadcast(news)
    Newspaper.new.print(news)
  end
end

class Newspaper
  def print(news)
    puts news
  end
end

laura = Newsperson.new
laura.broadcast("Some Breaking News!") # => "Some Breaking News!"
```
This is a functioning broadcast method, but right now it’s tied to the Newspaper object. What if we change the name of the Newspaper class? What if we add more broadcast platforms? Both of these things would cause us to have to change our Newsperson object as well. Even though we have a very small dependency on the type of broadcasting our newsperson will be doing, it breaks all three of the requirements we defined above of good code.

#### Correct use of the Dependency Inversion Principle in Ruby
```ruby
class Newsperson
  def broadcast(news, platform = Newspaper)
    platform.new.broadcast(news)
  end
end 

class Newspaper
  def broadcast(news)
    do_something_with news
  end
end

class Twitter
  def broadcast(news)
    tweets news
  end
end

class Television
  def broadcast(news)
    live_coverage news
  end
end 

laura = Newsperson.new
laura.broadcast("Breaking news!") #do_something_with "Breaking news!"
laura.broadcast("Breaking news!", Twitter) #tweets "Breaking news!"
```
As you can see, we can now pass any news broadcasting platform through the broadcast method whether that’s Twitter, TV, or Newspaper. And if we know that Newspapers are usually going to be the platform of choice, we can make newspaper the default platform. This second strategy is flexible, mobile, and not fragile. We can change any of these classes and we won’t break the other classes. The higher level Newsperson class does not depend on the lower level classes and vice versa.

For more info [The Dependency Inversion Principle (1996)](https://web.archive.org/web/20110714224327/http://www.objectmentor.com/resources/articles/dip.pdf)

## Conclusion

In conclusion, remember that SOLID principles themselves don’t guarantee great object-oriented design. Apply SOLID principles smartly. To do so, you need to know exactly what problem you’re trying to solve and if the problem is truly a risk for your system. For example, excessively segregating classes to conform with the SRP principle may lead to low cohesion and even performance losses.

However, if applied correctly, SOLID design recommendations can help you build system architecture that is easy to modify and extend over time, which is precisely what every developer should strive for. 

## References
[RubyCademy](https://medium.com/rubycademy/solid-ruby-in-5-short-examples-353ea22f9b05)

[SOLID wikipedia](https://en.wikipedia.org/wiki/SOLID)

[RubyGarage](https://rubygarage.org/blog/solid-principles-of-ood#article_title_1)

[SOLID Design Principles in Ruby by Anil Wadghule](https://www.youtube.com/watch?v=QqBb4gb7PM0/0.jpg)